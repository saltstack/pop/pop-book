==========
!ARCHIVED!
==========

This project has been archived, along with all other POP and Idem-based projects.

* For more details: `Salt Project Blog - POP and Idem Projects Will Soon be Archived <https://saltproject.io/blog/2025-01-24-idem-pop-projects-archived/>`__

====================================
Intro to Plugin Oriented Programming
====================================

This book in intended to give people a theoretical view of Plugin Oriented
Programming. The philosophy, reason, and rules, that will lead them to writing
good Plugin Oriented Programming code.

This is not intended as a feature by feature introduction to the pop library.
This documentation will be found in the pop documentation repository.
